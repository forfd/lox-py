# encoding: utf-8

import logging

from tokens import Token, TokenType


keywords = {
    "and": TokenType.AND,
    "class": TokenType.CLASS,
    "else": TokenType.ELSE,
    "false": TokenType.FALSE,
    "for": TokenType.FOR,
    "func": TokenType.FUNC,
    "if": TokenType.IF,
    "nil": TokenType.NIL,
    "or": TokenType.OR,
    "print": TokenType.PRINT,
    "super": TokenType.SUPER,
    "this": TokenType.THIS,
    "return": TokenType.RETURN,
    "true": TokenType.TRUE,
    "var": TokenType.VAR,
    "while": TokenType.WHILE,
    "switch": TokenType.SWITCH,
    "case": TokenType.CASE,
    "default": TokenType.DEFAULT,
}


class Scanner(object):
    """
    is used to read source code and produce tokens for parser.
    """

    def __init__(self, source):
        self.source = source
        self.tokens = []
        self.start = 0
        self.current = 0
        self.line = 0
        self.white_spaces = {' ', '\t', '\r'}

    def is_end(self):
        return self.current >= len(self.source)

    def advance(self):
        self.current += 1
        return self.source[self.current - 1]

    def skip_line_comment(self):
        while self.current < len(self.source) and self.peek() != '\n':
            self.advance()

    def skip_block_comment(self):
        """
        skip comment like /* /* */ ... */
        """
        nesting = 1
        while nesting > 0:
            if self.is_end():
                logging.error("Unterminated block comment")
                return

            if self.peek() == '\n':
                self.line += 1

            if self.peek() == '/' and self.peek_next() == '*':
                nesting += 1
                self.advance()
                self.advance()
                continue

            if self.peek() == '*' and self.peek_next() == '/':
                nesting -= 1
                self.advance()
                self.advance()
                continue

            self.advance()

    def peek(self):
        if self.is_end():
            return None

        return self.source[self.current]

    def peek_next(self):
        if self.current + 1 >= len(self.source):
            return None

        return self.source[self.current + 1]

    def add_token(self, token_type, literal=None):
        text = self.source[self.start:self.current]
        self.tokens.append(Token(token_type, text, literal, self.line))

    def match(self, expect):
        if self.is_end():
            return False

        if self.source[self.current] != expect:
            return False

        self.current += 1
        return True

    def get_str(self):
        while self.peek() != '"' and (not self.is_end()):
            if self.peek() == '\n':
                self.line += 1
            self.advance()

        if self.is_end():
            logging.error(
                "Unterminated string, at line: {line}".format(line=self.line))
            return

        self.advance()  # skip '"'
        self.add_token(TokenType.STRING,
                       self.source[self.start + 1:self.current - 1])

    def get_number(self):
        while self.peek().isdigit():
            self.advance()

        text = self.source[self.start:self.current]
        self.add_token(TokenType.NUMBER, int(text))

    def get_identifier(self):
        while is_alphanumberic(self.peek()):
            self.advance()

        text = self.source[self.start:self.current]
        tk = keywords.get(text, None)
        if tk is None:
            tk = TokenType.IDENTIFIER

        self.add_token(tk)

    def scan_token(self):
        token_types = {
            '(': TokenType.LEFT_PAREN,
            ')': TokenType.RIGHT_PAREN,
            '{': TokenType.LEFT_BRACE,
            '}': TokenType.RIGHT_BRACE,
            ',': TokenType.COMMA,
            '.': TokenType.DOT,
            '-': TokenType.MINUS,
            '+': TokenType.PLUS,
            ';': TokenType.SEMICOLON,
            '*': TokenType.STAR
        }

        char = self.advance()
        token_type = token_types.get(char)
        if token_type is not None:
            self.add_token(token_type)
            return

        tk = None
        if char == '!':
            tk = TokenType.BANG_EQUAL if self.match('=') else TokenType.BANG
            self.add_token(tk)
        elif char == '=':
            tk = TokenType.EQUAL_EQUAL if self.match('=') else TokenType.EQUAL
            self.add_token(tk)
        elif char == '<':
            tk = TokenType.LESS_EQUAL if self.match('=') else TokenType.LESS
            self.add_token(tk)
        elif char == '>':
            tk = TokenType.GREATER_EQUAL if self.match(
                '=') else TokenType.GREATER
            self.add_token(tk)

        elif char == '/':
            if self.match('/'):
                self.skip_line_comment()
            elif self.match('*'):
                self.skip_block_comment()
            else:
                self.add_token(TokenType.SLASH)

        elif char in self.white_spaces:
            return
        elif char == '\n':
            self.line += 1
            return
        elif char == '"':
            self.get_str()
        else:
            if char.isdigit():
                self.get_number()
            elif is_alpha(char):
                self.get_identifier()
            else:
                msg = 'Unexpected character: {char} at line: {line_num}'.format(
                    char=char, line_num=self.line)
                logging.error(msg)
                return

    def scan_tokens(self):
        while not self.is_end():
            self.start = self.current
            self.scan_token()

        self.tokens.append(Token(TokenType.EOF, "", None, self.line))
        return self.tokens


def is_alpha(c):
    return c == '_' or c.isalnum()


def is_alphanumberic(c):
    return is_alpha(c) or c.isdigit()


if __name__ == "__main__":
    print("Scanner")

    source = '.({+, -, *}).==,!=,>,>=,<,<='
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-----------get string------------")

    source = '({"strstr"})'
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-----------get number&string------------")

    source = '({ 996691 , "Hello" })'
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-------------get line comment-------------")
    source = '"this is a string", 123456, // this is a comment'
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-------------get block comment-------------")
    source = """
    "this is a string"  // this is a line comment

    123456,

    /*  start comment

    this is a block comment
    /*
    this is a nested block comment,
    /**/
    */
    end comment
    */
    """
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-------------get identifier-------------")
    source = "var abc=1; var def=123;"
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-------------get for loop tokens-------------")
    source = """
    var i = 0;
    for i < 10; i++; {
        print(i);
    }
    """
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)

    print("-------------get switch tokens-------------")
    source = """
    var x = "123";
    switch x {
    case "123":
        print(123);
    case "456":
        print(456);
    default:
        print(-1);
    }
    """
    scanner = Scanner(source)
    for tk in scanner.scan_tokens():
        print(tk)
