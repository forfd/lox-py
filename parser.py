# encoding: utf-8

import logging

from tokens import TokenType


class Parser(object):
    """
    a recursive descent parser

    A parser is a software component that takes input data (frequently text) and builds a data structure 
    – often some kind of parse tree, 
      abstract syntax tree or other hierarchical structure, 
      giving a structural representation of the input while checking for correct syntax
    """

    def __init__(self, tokens):
        self.tokens = tokens
        self.current = 0

    def peek(self):
        return self.tokens[self.current]

    def is_end(self):
        return self.peek().token_type == TokenType.EOF

    def check(self, tk):
        if self.is_end():
            return False
        return self.peek().token_type == tk

    def previous(self):
        return self.tokens[self.current-1]

    def advance(self):
        if not self.is_end():
            self.current += 1

        return self.previous()

    def match(self, *tokens):
        for tk in tokens:
            if self.check(tk):
                self.advance()
                return True

        return False

    def consume(self, tk, msg):
        if self.check(tk):
            return self.advance()

        logging.error(tk, msg)
        raise Exception("Parser token error", tk, msg)

    def parse(self):
        try:
            return self.expression()
        except Exception as identifier:
            logging.error(identifier)
            return None

    def expression(self):
        return self.equality()

    def equality(self):
        return


if __name__ == "__main__":
    print("Parser")
