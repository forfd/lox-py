# encoding: utf-8


class Unary(object):
    def __init__(self, symbol, value):
        self.symbol = symbol
        self.value = value

    def accept(self, calculator):
        return calculator.eval_unary(self)


class Binary(object):
    def __init__(self, symbol, left_operand, right_operand):
        self.symbol = symbol
        self.left_operand = left_operand
        self.right_operand = right_operand

    def accept(self, calculator):
        return calculator.eval_binary(self)


class Calculator(object):
    def __init__(self, stmts):
        self.stmts = stmts

    def eval_stmts(self):
        for stmt in self.stmts:
            v = stmt.accept(self)
            print(v)

    def calculate(self, exp):
        try:
            return exp.accept(self)
        except AttributeError as identifier:
            print(identifier)
            return int(exp)

    def eval_unary(self, unary):
        if unary.symbol == "" or unary.symbol == "+":
            return int(unary.value)

        return -1 * int(unary.value)

    def eval_binary(self, binary):
        op1 = self.calculate(binary.left_operand)
        op2 = self.calculate(binary.right_operand)
        if binary.symbol == '+':
            return int(op1) + int(op2)
        elif binary.symbol == '-':
            return int(op1) - int(op2)
        elif binary.symbol == '*':
            return int(op1) * int(op2)
        elif binary.symbol == "/":
            return int(op1) / int(op2)
        else:
            raise Exception("unsupport operator")


if __name__ == "__main__":
    stmts = [
        Unary('-', 10),
        Unary('', 100),
        Binary('+', 1, 100),
        Binary('*', 100, 100),
        Binary('/', 100, 2),
        Binary('/', Unary('+', 100), Unary('', 2)),
        Binary('/', Binary('+', 20, 30), Binary('*', 2, 5)),
    ]

    Calculator(stmts).eval_stmts()
